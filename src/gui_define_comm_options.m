function fHandle= gui_define_comm_options(comm, comm_name)
% function fHandle= gui_define_comm_options(comm, comm_name)
% interface for defining options for following DYNARE commands: dynare,
% stoch_simul, simul, estimation and conditional dorecast
%
% INPUTS
%   comm:         dynare_gui_ structure where options for command are saved
%   comm_name:    command name (dynare, stoch_simul, simul, estimation or conditional forecast)
%
% OUTPUTS
%   fHandle:    handle of Matlab figure which displays the interface
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2003-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global model_settings project_info

if ~isfield(model_settings, comm_name) || isempty(model_settings.(comm_name))
    model_settings.(comm_name) = struct();
end
user_defined_options = model_settings.(comm_name);

bg_color = char(getappdata(0,'bg_color'));
special_color = char(getappdata(0,'special_color'));

fHandle = figure('Name', 'Dynare GUI - Command definition', ...
    'NumberTitle', 'off', 'Units', 'characters','Color', [.941 .941 .941], ...
    'Position', [10 10 180 36], 'Visible', 'off', 'Resize', 'off','WindowStyle','modal');

movegui(fHandle,'center');
fHandle.Visible = 'On';

names = fieldnames(comm);
num_groups = size(names,1);
current_option = 1;
maxDisplayed = 11;
handles = [];
top = 32;

uicontrol( ...
    'Parent', fHandle, ...
    'Style', 'text', ...
    'Units', 'characters', 'BackgroundColor', bg_color,...
    'Position', [2 34 75 1.5], ...
    'FontWeight', 'bold', ...
    'String', sprintf('Define %s command options:',comm_name), ...
    'HorizontalAlignment', 'left');

handles.uipanel = uipanel( ...
    'Parent', fHandle, ...
    'Tag', 'uipanelShocks', ...
    'Units', 'characters', 'BackgroundColor', special_color,...
    'Position', [2 5 178 28], ...
    'Title', '','BorderType', 'none');

% Create tabs
handles.tab_group = uitabgroup(handles.uipanel,'Position',[0 0 1 1]);

for idx = 1:num_groups
    create_tab(idx, names{idx});
end

%handles.tab_group.SelectedChild = 1;
% --- PUSHBUTTONS -------------------------------------
handles.pushbuttonUseOptions = uicontrol( ...
    'Parent', fHandle, ...
    'Style', 'pushbutton', ...
    'Units', 'characters', ...
    'Position', [2 1 25 2], ...
    'String', 'Use these options', ...
    'Callback', @pushbuttonUseOptions_Callback);

handles.pushbuttonReset = uicontrol( ...
    'Parent', fHandle, ...
    'Style', 'pushbutton', ...
    'Units', 'characters', ...
    'Position', [29 1 25 2], ...
    'String', 'Clear options', ...
    'Callback', @pushbuttonReset_Callback);

    function create_tab(num, group_name)
        new_tab = uitab(handles.tab_group, 'Title', group_name, 'UserData', num);

        tabs_panel = uipanel('Parent', new_tab,'BackgroundColor', 'white', 'BorderType', 'none');
        group = comm.(group_name);
        numOptions = size(group,1);
        tab_handles = [];
        width_name = 40;
        width_value = 20;
        width_default = 20;
        width_description = 55;
        h_space = 3;

        % Create slider
        if numOptions > maxDisplayed
            sld = uicontrol('Style', 'slider',...
                'Parent', tabs_panel, ...
                'Min',0,'Max',numOptions - maxDisplayed,'Value',numOptions - maxDisplayed ,...
                'Units', 'characters','Position', [172.2 -0.2 3 25], ...
                'SliderStep', [0.1 0.3]);%, ...
                %'Callback', {@scrollPanel_Callback});
                try    % R2013b and older
                    hListener = addlistener(sld, 'ActionEvent', @scrollPanel_Callback);
                catch  % R2014a and newer
                    hListener = addlistener(sld, 'ContinuousValueChange', @scrollPanel_Callback);
                end
                setappdata(sld, 'sliderListener', hListener);
        end
        uicontrol( ...
            'Parent',  tabs_panel, ...
            'Style', 'text', ...
            'Units', 'characters', 'BackgroundColor', special_color,...
            'Position', [h_space top-9 width_name 1.5], ...
            'FontWeight', 'bold', ...
            'String', 'Command option:', ...
            'HorizontalAlignment', 'left');

        uicontrol( ...
            'Parent',  tabs_panel, ...
            'Style', 'text', ...
            'Units', 'characters', 'BackgroundColor', special_color,...
            'Position', [h_space*2+width_name top-9 width_value 1.5], ...
            'FontWeight', 'bold', ...
            'String', 'Define new value:', ...
            'HorizontalAlignment', 'left');

        uicontrol( ...
            'Parent',  tabs_panel, ...
            'Style', 'text', ...
            'Units', 'characters', 'BackgroundColor', special_color,...
            'Position', [h_space*3+width_name+width_value top-9 width_default 1.5], ...
            'FontWeight', 'bold', ...
            'String', 'Default value:', ...
            'HorizontalAlignment', 'left');

        uicontrol( ...
            'Parent', tabs_panel, ...
            'Style', 'text', ...
            'Units', 'characters', 'BackgroundColor', special_color,...
            'Position', [h_space*4+width_name+width_value*2 top-9 width_description 1.5], ...
            'FontWeight', 'bold', ...
            'String', 'Description:', ...
            'HorizontalAlignment', 'left');

        new_top = top - 9.5;
        visible = 'on';
        for ii = 1:size(group,1)
            if ii > maxDisplayed
                visible = 'off';
            end
            tab_handles.options(ii) = uicontrol( ...
                'Parent',  tabs_panel, ...
                'Style', 'text', ...
                'Units', 'characters', 'BackgroundColor', special_color,...
                'Position', [h_space new_top-ii*2 width_name 1.5], ...
                'String', group{ii,1}, ... %'UserData', group{ii,5}, ...
                'HorizontalAlignment', 'left',...
                'Visible', visible);

            option_type = group{ii,3};
            if strcmp(option_type, 'check_option')
                tab_handles.values(ii) = uicontrol( ...
                    'Parent', tabs_panel, ...
                    'Style', 'checkbox', ...
                    'Units', 'characters', 'BackgroundColor', special_color,...
                    'Position', [h_space*2+width_name new_top-ii*2 width_value 1.5], ...
                    'Value', 0,...
                    'TooltipString', option_type,...
                    'HorizontalAlignment', 'left',...
                    'Visible', visible);
            elseif strcmp(option_type, 'popupmenu')
                % commented in commit after a290b22 because `popupOptions`
                % variable does not exist
%                 tab_handles.values(ii) = uicontrol( ...
%                     'Parent', tabs_panel, ...
%                     'Style', 'popupmenu', ...
%                     'String', popupOptions, ...
%                     'Units', 'characters', 'BackgroundColor', special_color,...
%                     'Position', [h_space*2+width_name new_top-ii*2 width_value 1.5], ...
%                     'TooltipString', option_type,...
%                     'Value', 1, ...
%                     'HorizontalAlignment', 'left',...
%                     'Visible', visible);
            elseif iscell(option_type)
                tab_handles.values(ii) = uicontrol('Parent', tabs_panel, ...
                    'Style', 'popup',...
                    'String', option_type,...
                    'Value', 1, ...
                    'Units','characters',  'Position',[h_space*2+width_name new_top-ii*2 width_value 1.5],...
                    'HorizontalAlignment', 'left',...
                    'TooltipString', 'popup_value',...
                    'Visible', visible);
            else
                tab_handles.values(ii) = uicontrol( ...
                    'Parent', tabs_panel, ...
                    'Style', 'edit', ...
                    'Units', 'characters', 'BackgroundColor', special_color,...
                    'Position', [h_space*2+width_name new_top-ii*2 width_value 1.5], ...
                    'String', '',...
                    'TooltipString', option_type,...
                    'HorizontalAlignment', 'left',...
                    'Visible', visible,...
                    'Callback',{@checkUserInput_Callback, group{ii,1}, option_type});
                if strcmp(option_type, 'FILENAME')
                    uicontrol('Parent', tabs_panel,'Style','pushbutton',...
                        'String', '...',...
                        'Units','characters',...
                        'Position', [h_space*2+width_name+width_value-3 new_top-ii*2 3 1.5], ...
                        'Callback',{@select_file, group{ii,1}, tab_handles.values(ii)});
                end
            end
            if isfield(user_defined_options, group{ii,1})
                if ischar(option_type)
                    if strcmp(option_type, 'check_option')
                        assert(user_defined_options.(group{ii,1}) == 0 || user_defined_options.(group{ii,1}) == 1);
                        tab_handles.values(ii).Value = user_defined_options.(group{ii,1});
                    elseif strcmp(option_type, 'INTEGER') || strcmp(option_type, 'DOUBLE')
                        tab_handles.values(ii).Value = user_defined_options.(group{ii,1});
                        tab_handles.values(ii).String = num2str(user_defined_options.(group{ii,1}), 15);
                    elseif strcmp(option_type, '[INTEGER1:INTEGER2]') ...
                            || strcmp(option_type, 'INTEGER or [INTEGER1:INTEGER2]') ...
                            || strcmp(option_type, '[DOUBLE DOUBLE]') ...
                            || strcmp(option_type, '[DOUBLE DOUBLE DOUBLE]')
                        tab_handles.values(ii).Value = str2double(user_defined_options.(group{ii,1}));
                        tab_handles.values(ii).String = user_defined_options.(group{ii,1});
                    elseif strcmp(option_type, 'eps, pdf, fig, none') ...
                            || strcmp(option_type, 'FILENAME') ...
                            || strcmp(option_type, 'VARIABLE_LIST') ...
                            || strcmp(option_type, '( VARIABLE_NAME [[,] VARIABLE_NAME ...] )')
                        tab_handles.values(ii).String = user_defined_options.(group{ii,1});
                    end
                elseif iscell(option_type)
                    tab_handles.values(ii).Value = user_defined_options.(group{ii,1});
                else
                    error('error with option_type');
                end
            end

            defaultString = group{ii,2};
            if strcmp(option_type, 'popupmenu')
                %defaultString = popupOptions{userDefaultValue};
            end
            if ~ischar(defaultString) && isnumeric(defaultString)
                defaultString = num2str(defaultString);
            end
            if length(defaultString) > width_default
                defaultString = strcat(defaultString(1:width_default-3),'...');
            end
            tab_handles.defaults(ii) = uicontrol( ...
                'Parent',  tabs_panel, ...
                'Style', 'text', ...
                'Units', 'characters', 'BackgroundColor', special_color,...
                'Position', [h_space*3+width_name+width_value new_top-ii*2 width_default 1.5], ...
                'String', defaultString, 'TooltipString', defaultString,...
                'HorizontalAlignment', 'left',...
                'Visible', visible);
            % if this isn't here, the default string isn't inserted. figure
            % out why later
            tab_handles.defaults(ii).String = defaultString;

            textDesc = group{ii,4};
            if length(textDesc) > width_description
                textDesc = strcat(textDesc(1:width_description-5),'...');
                i = 1;
                t_width = 50;
                text = group{ii,4};
                s = size(text,2);
                tool_tip_string = '';
                while i < s
                    if s-i >= t_width
                        str = text(1:t_width);
                        j = 1;
                        i = i+t_width +j-1;
                        text = text(t_width+j:end);
                    else
                        str = text;
                        i = s;
                        text = '';
                    end
                    tool_tip_string = sprintf('%s\n%s',tool_tip_string, str);
                end
            else
                tool_tip_string = textDesc;
            end

            tab_handles.description(ii)=uicontrol( ...
                'Parent',  tabs_panel, ...
                'Style', 'text', ...
                'Units', 'characters', 'BackgroundColor', special_color,...
                'Position', [h_space*4+width_name+width_value*2 new_top-ii*2 width_description 1.5], ...
                'String', textDesc, ...
                'HorizontalAlignment', 'left',...
                'Visible', visible);
            set(tab_handles.description(ii), 'TooltipString', tool_tip_string);

            handles.options(current_option) = tab_handles.options(ii);
            handles.values(current_option) = tab_handles.values(ii);
            current_option = current_option + 1;
        end

        function select_file(~, ~, option_name, uicontrol)
            try
                file_types = {'*.*'};
                switch option_name
                    case 'datafile'
                        file_types = {'*.m';'*.mat';'*.xls';'*.xlsx';'*.csv'};
                    case 'mode_file'
                        file_types = {'*.mat'};
                    case 'mcmc_jumping_covariance_file'
                        file_types = {'*.mat'};
                end

                [fileName,pathName] = uigetfile(file_types,sprintf('Select %s ...', option_name));

                if fileName == 0
                    return
                end
                project_folder= project_info.project_folder;
                if strcmp([pathName,fileName], [project_folder,filesep,fileName]) ~= 1
                    [status, message] = copyfile([pathName,fileName],[project_folder,filesep,fileName]);
                    if status
                        uiwait(msgbox('File copied to project folder', 'DynareGUI','modal'));
                    else
                        gui_tools.show_error(['Error while copying file to project folder: ', message]);
                    end
                end
                set(uicontrol, 'String', fileName);
            catch ME
                gui_tools.show_error(['Error while selecting ',option_name], ME, 'basic');
            end
        end

        function checkUserInput_Callback(hObject, ~, option_name, option_type)
            value = get(hObject, 'String');
            if isempty(value)
                return
            end
            switch option_type
                case {'INTEGER', 'INTEGER or [INTEGER1:INTEGER2]','[INTEGER1:INTEGER2]', 'DOUBLE', '[DOUBLE DOUBLE]', '[DOUBLE DOUBLE DOUBLE]', '[INTEGER1 INTEGER2 ...]', ...
                        'INTEGER or [INTEGER1:INTEGER2] or [INTEGER1 INTEGER2 ...]', '[INTEGER1:INTEGER2] or [INTEGER1 INTEGER2 ...]'}
                    [num, status] = str2num(value);
                    if strcmp(option_type,'[DOUBLE DOUBLE]')
                        if(size(num,1)~= 1 || size(num,2)~= 2)
                            status = 0;
                        end
                    end
                    if strcmp(option_type,'[DOUBLE DOUBLE DOUBLE]')
                        if(size(num,1)~= 1 || size(num,2)~= 3)
                            status = 0;
                        end
                    end
                    if strcmp(option_type,'[INTEGER1 INTEGER2 ...]')
                        if(size(num,1)~= 1 || size(num,2) < 2)
                            status = 0;
                        end
                    end
                    if strcmp(option_type,'INTEGER')
                        if(size(num,1)~= 1 || size(num,2) ~= 1 || floor(num)~=num)
                            status = 0;
                        end
                    end
                    if strcmp(option_type,'[INTEGER1:INTEGER2]')
                        if(size(num,1)~= 1 || size(num,2) ~= 2 || floor(num)~=num)
                            status = 0;
                        end
                    end

                    if ~status
                        warnStr = sprintf('Not valid input! Please define option %s as %s',option_name, option_type );
                        gui_tools.show_warning(warnStr);
                        set(hObject, 'String','');
                    end
                case '(NAME, VALUE, ...)'
                    %TODO optim
            end
        end

        function scrollPanel_Callback(hObject, ~)
            value = get(hObject, 'Value');
            value = floor(value);
            move = numOptions - maxDisplayed - value;
            for spi = 1:numOptions
                if spi <= move || spi > move+maxDisplayed
                    visible = 'off';
                    set(tab_handles.options(spi), 'Visible', visible);
                    set(tab_handles.values(spi), 'Visible', visible);
                    set(tab_handles.defaults(spi), 'Visible', visible);
                    set(tab_handles.description(spi), 'Visible', visible);
                else
                    visible = 'on';
                    set(tab_handles.options(spi), 'Visible', visible);
                    set(tab_handles.options(spi), 'Position', [h_space new_top-(spi-move)*2 width_name 1.5]);
                    set(tab_handles.values(spi), 'Visible', visible);
                    set(tab_handles.values(spi), 'Position', [h_space*2+width_name new_top-(spi-move)*2 width_value 1.5]);
                    set(tab_handles.defaults(spi), 'Visible', visible);
                    set(tab_handles.defaults(spi), 'Position',[h_space*4+width_name+width_value*2 new_top-(spi-move)*2 width_default 1.5]);
                    set(tab_handles.description(spi), 'Visible', visible);
                    set(tab_handles.description(spi), 'Position',[h_space*4+width_name+width_value*3 new_top-(spi-move)*2 width_description 1.5]);
                end
            end
        end
    end

    function pushbuttonUseOptions_Callback(~, ~)
        model_settings.(comm_name) = saveUserOptions();
        setappdata(0, comm_name, model_settings.(comm_name));
        close;
    end

    function new_user_options = saveUserOptions()
        new_user_options = struct();
        for ii = 1:size(handles.values, 2)
            option_type = handles.values(ii).TooltipString;
            if strcmp(option_type, 'check_option')
                if handles.values(ii).Value
                    new_user_options.(get(handles.options(ii),'String')) = handles.values(ii).Value;
                end
            else
                value = strtrim(handles.values(ii).String);
                if ~isempty(value)
                    comm_option = handles.options(ii).String;
                    if strcmp(option_type, 'INTEGER') || strcmp(option_type, 'DOUBLE')
                        new_user_options.(comm_option) = str2double(value);
                    elseif strcmp(option_type, 'popupmenu') || strcmp(option_type, 'popup_value')
                        if handles.values(ii).Value ~= 1
                            new_user_options.(comm_option) = handles.values(ii).Value;
                        end
                    else
                        new_user_options.(comm_option) = value;
                    end
                end
            end
        end
    end

    function pushbuttonReset_Callback(~, ~)
        for ii = 1:size(handles.values,2)
            option_type = get(handles.values(ii), 'TooltipString');
            if strcmp(option_type, 'check_option')
                set(handles.values(ii), 'Value', 0);
            else
                set(handles.values(ii), 'String', '');
            end
        end
    end
end