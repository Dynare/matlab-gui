function gui_close_project(clear_dynare_vars)
% function gui_close_project()
% closes current project Dynare_GUI project file
%
% INPUTS
%   none
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2003-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global project_info

gui_tools.project_log_entry('Project Close', ['project_name = ' project_info.project_name '; project_folder=' project_info.project_folder])

if project_info.modified
    answer = questdlg(['Do you want to save changes to project ' project_info.project_name '?'], 'DynareGUI', 'Yes', 'No', 'Cancel', 'Yes');
    if strcmp(answer, 'Yes')
        gui_tools.save_project();
    elseif strcmp(answer, 'Cancel')
        return
    end
end

%close all opened tabs
gui_tabs.close_all();

%disable menu options
gui_set_menus(false);

evalin('base', 'clear project_info model_settings');
if clear_dynare_vars
    evalin('base', 'clear M_ options_ oo_ estim_params_ bayestopt_ dataset_ dataset_info estimation_info ys0_ ex0_');
    clear_persistent_variables(fileparts(which('dynare')), true)
end

% remove appdata
if isappdata(0, 'estimation')
    rmappdata(0,'estimation')
end

if isappdata(0, 'stoch_simul')
    rmappdata(0,'stoch_simul')
end

if isappdata(0, 'estim_params')
    rmappdata(0,'estim_params')
end

if isappdata(0, 'varobs')
    rmappdata(0,'varobs')
end

evalin('base','diary off;');

end
